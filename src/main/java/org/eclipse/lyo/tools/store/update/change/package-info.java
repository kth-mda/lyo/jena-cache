/**
 * Classes for defining changes for handlers.
 * {@link org.eclipse.lyo.tools.store.update.change.HistoryResource} can be persisted in a
 * triplestore.
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.18.0
 */
package org.eclipse.lyo.tools.store.update.change;
