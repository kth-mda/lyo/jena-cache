/**
 * Common primitives for updating store.
 * {@link org.eclipse.lyo.tools.store.update.StoreUpdateRunnable} is scheduled by
 * {@link org.eclipse.lyo.tools.store.update.StoreUpdateManager}.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.18.0
 */
package org.eclipse.lyo.tools.store.update;
