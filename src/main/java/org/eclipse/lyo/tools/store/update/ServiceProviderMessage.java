package org.eclipse.lyo.tools.store.update;

import java.net.URI;

/**
 * Created on 06.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public interface ServiceProviderMessage {
    URI getServiceProviderId();
}
